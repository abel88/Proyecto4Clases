package Clases;

/**
 * Created by Ventas on 5/07/2017.
 */
public class Proveedor {
    public  String nombreProv;
    public  int edadProv;
    public String direccionProv;
    Producto producto;
    public  Proveedor(){

    }

    public String getNombreProv() {
        return nombreProv;
    }

    public void setNombreProv(String nombreProv) {
        this.nombreProv = nombreProv;
    }

    public int getEdadProv() {
        return edadProv;
    }

    public void setEdadProv(int edadProv) {
        this.edadProv = edadProv;
    }

    public String getDireccionProv() {
        return direccionProv;
    }

    public void setDireccionProv(String direccionProv) {
        this.direccionProv = direccionProv;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
}
